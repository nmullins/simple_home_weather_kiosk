$(document).ready(function() {
    var url = "http://api.openweathermap.org/data/2.5/weather?zip=48125,us&units=imperial&APPID=7e3fa6db41ce0aa5e9148d74c50d4084";
    
    setInterval(getCurrentWeather, 3600000);
    
    function getCurrentWeather() {
        $.ajax({
            url: url,
            type: 'GET',
            data: {},
            success: function(result) {
                //console.log(result);
                
                if  (result.cod === 200 || result.cod == '200') {
                    structureWeatherData(result);
                }
            },
        });    
    }
    
    function structureWeatherData(weather) {
        var currentTemp = weather.main.temp;
        var lowTemp = weather.main.temp_min;
        var highTemp = weather.main.temp_max;
        
        setTemp(currentTemp, lowTemp, highTemp);
        
        var windSpeed = weather.wind.speed;
        var windDirection = weather.wind.deg;
        
        setWind(windSpeed, windDirection);
        
        if (typeof weather.precipitation  !== 'undefined' || weather.precipitation === null) {
            var precipitation = weather.precipitation.mode;
        }
        else {
           var precipitation = 'Unknown'; 
        }
        setPrecip(precipitation);
        
    }
    
    function setPrecip(precip) {
        $(".precipitation").html(precip);
    }
    
    function setTemp(curr, low, high) {
        $(".current_temp").html(curr + "&deg;F");
        $(".low_temp").html(low + "&deg;F");
        $(".high_temp").html(high + "&deg;F");
        
        setTempColors(curr, low, high);
    }
    
    function setWind(speed, dir) {
        var cardinal_direction;
        if (dir > 303.75 && dir <= 348.75) {
            cardinal_direction = 'NW';
        }
        else if (dir > 258.75 && dir <= 303.75) {
            cardinal_direction = 'W';
        }
        else if (dir > 213.75 && dir <= 258.75) {
            cardinal_direction = 'SW';
        }
        else if (dir > 168.75 && dir <= 213.75) {
            cardinal_direction = 'S';
        }
        else if (dir > 123.75 && dir <= 168.75 ) {
            cardinal_direction = 'SE';
        }
        else if (dir > 78.75 && dir <= 123.75) {
            cardinal_direction = 'E';
        }
        else if ( dir > 33.75 && dir <= 78.75) {
            cardinal_direction = 'NE';
        }
        else {
            cardinal_direction = 'N';
        }
        
        $(".wind_speed_and_direction").html(speed + " MPH" +  "  " + cardinal_direction);
    }
    
    function setTempColors(c, l, h) {
        if (c < 32) {
            $(".current_temp").css("background-color", "#0000e6");
            $(".current_temp").css("border-color", "#0000e6");
        }
        else if (c >= 32 && c < 60) {
            $(".current_temp").css("background-color", "#8080ff");
            $(".current_temp").css("border-color", "#8080ff");
        }
        else if (c >= 60 && c < 75) {
            $(".current_temp").css("background-color", "#00cc00");
            $(".current_temp").css("border-color", "#00cc00");
        }
        else if (c >= 75 && c < 85) {
            $(".current_temp").css("background-color", "#ff9900");
            $(".current_temp").css("border-color", "#ff9900");
        }
        else {
            $(".current_temp").css("background-color", "#ff0000");
            $(".current_temp").css("border-color", "#ff0000");
        }
        
        if (l < 32) {
            $(".low_temp").css("background-color", "#0000e6");
            $(".low_temp").css("border-color", "#0000e6");
        }
        else if (l >= 32 && l < 60) {
            $(".low_temp").css("background-color", "#8080ff");
            $(".low_temp").css("border-color", "#8080ff");
        }
        else if (l >= 60 && l < 75) {
            $(".low_temp").css("background-color", "#00cc00");
            $(".low_temp").css("border-color", "#00cc00");
        }
        else if (l >= 75 && l < 85) {
            $(".low_temp").css("background-color", "#ff9900");
            $(".low_temp").css("border-color", "#ff9900");
        }
        else {
            $(".low_temp").css("background-color", "#ff0000");
            $(".low_temp").css("border-color", "#ff0000");
        }
        
        if (h < 32) {
            $(".high_temp").css("background-color", "#0000e6");
            $(".high_temp").css("border-color", "#0000e6");
        }
        else if (h >= 32 && h < 60) {
            $(".high_temp").css("background-color", "#8080ff");
        }
        else if (h >= 60 && h < 75) {
            $(".high_temp").css("background-color", "#00cc00");
            $(".high_temp").css("border-color", "#00cc00");
        }
        else if (h >= 75 && h < 85) {
            $(".high_temp").css("background-color", "#ff9900");
            $(".high_temp").css("border-color", "#ff9900");
        }
        else {
            $(".high_temp").css("background-color", "#ff0000");
            $(".high_temp").css("border-color", "#ff0000");
        }
    }
    
    getCurrentWeather();
});