$(document).ready(function() {
    setInterval(startTime, 500);
    setInterval(startDate, 3600000);
    var color = 'black';
    
    function startTime() {
        var time_of_day;
        var today = new Date();
        
        var h = today.getHours();
        if (h > 12) {
            time_of_day = 'PM';
            h = h - 12;
        }
        else if (h === 12) {
            time_of_day = 'PM';
        }
        else {
            if (h === 00 || h === 0) {
                h = 12;
            }
            time_of_day = 'AM';
        }
        
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        $(".clock").html(h + ":" + m + ":" + "<span id='seconds'>" + s + "</span> " + time_of_day);
        toggleColor();
    }
    
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
    
    function toggleColor() {
        if (color == 'black') {
            $("#seconds").css("color", "#000000");
            $("#seconds").css("background-color", "#fffffff");
            color = 'white';
        }
        else {
            $("#seconds").css("color", "#ffffff");
            $("#seconds").css("background-color", "#000000");
            color='black';
        }
    }
    
    function startDate() {
        var today = new Date();
        var dow = today.getDay();
        var day = today.getDate();
        var month = today.getMonth();
        var year = today.getFullYear();
        
        $(".date").html(setDateString(dow, day, month, year));
    }
    
    function setDateString(dow, d, m, y) {
        var dow_array = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var month_array = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        
        var date_string = dow_array[dow] + ', ' + month_array[m] + ' ' + d + ' ' + y;
        
        return date_string;
    }
    
    startTime();
    startDate();
});
